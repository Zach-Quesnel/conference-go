import json
import requests
from .keys import PEXELS_API_KEY, OPENWEATHER_API_KEY


def get_photo(city, state):
    # use requests to get a picture from pexel
    url = "https://api.pexels.com/v1/search"
    parameters = {"per_page": 1, "query": city + " " + state}
    headers = {"Authorization": PEXELS_API_KEY}
    response = requests.get(
        url, params=parameters, headers=headers
    )  # may need params=parameters
    content = json.loads(response.content)

    # create a dictionary that has the picture URL
    try:
        return {
            "picture_url": content["photos"][0]["src"]["original"],
        }

    except (KeyError, IndexError):
        return {"picture_url": None}


def get_weather_data(city, state):
    # make request to openweather to get lat and lon based off city
    url = "http://api.openweathermap.org/geo/1.0/direct"
    parameters = {"q": city + "," + state + ",1", "appid": OPENWEATHER_API_KEY}
    response = requests.get(url, params=parameters)
    content = json.loads(response.content)

    try:
        coordinates = {"lat": content[0]["lat"], "lon": content[0]["lon"]}

    except (KeyError, IndexError):
        return {"lat or lon": None}

    # make a request using the lat and lon to get current weather
    url = "https://api.openweathermap.org/data/2.5/weather"
    parameters = {
        "lat": coordinates["lat"],
        "lon": coordinates["lon"],
        "units": "imperial",
        "appid": OPENWEATHER_API_KEY,
    }
    response = requests.get(url, params=parameters)
    content = json.loads(response.content)

    try:
        return {
            "temp": content["main"]["temp"],
            "description": content["weather"][0]["description"],
        }

    except (KeyError, IndexError):
        return {"temp or weather": None}
