import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()

while True:
    try:

        def process_approval(ch, method, properties, body):

            message = json.loads(body)
            presenter_name = message["presenter_name"]
            title = message["title"]
            presenter_email = message["presenter_email"]
            message_body = f"{presenter_name}, we're happy to tell you that your presentation {title} has been accepted"

            send_mail(
                "Your presentation has been accepted",
                message_body,
                "admin@conference.go",
                [presenter_email],
                fail_silently=False,
            )

        def process_rejection(ch, method, properties, body):

            message = json.loads(body)
            presenter_name = message["presenter_name"]
            title = message["title"]
            presenter_email = message["presenter_email"]
            message_body = f"{presenter_name}, we're sorry to tell you that your presentation {title} has been rejected"

            send_mail(
                "Your presentation has been rejected",
                message_body,
                "admin@conference.go",
                [presenter_email],
                fail_silently=False,
            )

        parameters = pika.ConnectionParameters(host="rabbitmq")
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.queue_declare(queue="presentation_approvals")
        channel.basic_consume(
            queue="presentation_approvals",
            on_message_callback=process_approval,
            auto_ack=True,
        )
        channel.queue_declare(queue="presentation_rejections")
        channel.basic_consume(
            queue="presentation_rejections",
            on_message_callback=process_rejection,
            auto_ack=True,
        )
        channel.start_consuming()

    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)


# commands to run to send email. after these commands, change the status in insomnia and it will send the email
# docker run -d  --name rabbitmq --hostname rabbitmq --network conference-go rabbitmq:3


# docker run -d -v "$(pwd)/monolith:/app" -p 8000:8000 --network conference-go conference-go-dev

# docker run -d -p "3000:8025" -e "MH_SMTP_BIND_ADDR=0.0.0.0:25" --name mail --network conference-go mailhog/mailhog

# docker run -it --network conference-go -v "$(pwd)/presentation_workflow:/app" presentation-workflow-dev bash

# python presentation_mailer/consumer.py
